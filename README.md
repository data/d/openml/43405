# OpenML dataset: Covid-19-Turkey-Daily-Details-Dataset

https://www.openml.org/d/43405

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
TurkeyCovid 19 Dataset
Data Source : https://covid19.saglik.gov.tr/
Content
This data set has been created in accordance with the data shared by the Ministry of Health of the Republic of Turkey. Data from the website of the ministry of health every day is added to the data set using the data mining method. I update the data set every day and share it on github. https://github.com/birolemekli/covid19-turkey-daily-details-dataset
Columns
totalTests: number of tests carried out up to
totalCases: number of cases announced up to day
totalDeaths: number of death announced up to day
totalIntensiveCare: the number of people in intensive care announced until the day
totalIntubated: number of intubated people announced until the day
totalRecovered: total healing people
dailyTest: the number of people tested daily
dailyCases: daily number of cases
dailyDeaths: daily deaths

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43405) of an [OpenML dataset](https://www.openml.org/d/43405). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43405/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43405/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43405/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

